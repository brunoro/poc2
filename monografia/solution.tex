The key idea of the solution is, given that Elixir uses strict evaluation and that every
expression has a value, crawl into the abstract syntax tree (AST) of a program and 
interactively fold the tree until the final program value is obtained.
By using automatic instrumentation and doing no reverse engineering on the language, the
debugger is kept portable across architectures and language implementations, and would benefit from 
any current and future compiler optimizations and bugfixes without any modifications.

% Design goals
Some features of the Elixir language require special attention when designing a debugger:
\begin{enumerate}
  %% Distributed systems
  \item Erlang and Elixir programs often are composed of thousands of asynchronous and isolated threads.
        Ways of inspecting and interact with multiple processes are needed in order to isolate issues
        on distributed systems.
  %% Macros
  \item The macro system allows to the language to be extended, resulting in more clean and organized code.
        A debugger should be aware of user-defined macros, making them explicitly expanded as necessary.
  %% Versions
  \item BEAM provides mechanisms for hot-code swapping, allowing for systems to be
        updated with no service interruption. 
        The debugger should be able to handle processes running multiple versions of the same module.
\end{enumerate}

\subsection{Architecture}
%% Companion
The debugger uses a \texttt{Companion} process to
store runtime information and control each debugged process.
Every expression being evaluated on the client is authorized on its \texttt{Companion},
which holds a list of breakpoints to be matched.
The \texttt{Companion} also keeps track of the binding and scope of the execution on the
client processes, providing the necessary information for the \emph{eval} calls.
The structure uses the \texttt{push\_stack} and \texttt{pop\_stack} messages to organize client
scope changes.

%% PIDTable 
Client processes can run code on different modules through function calls, meaning that 
debugged and non-debugged modules could run on a same process. 
Addressing this issue requires a way to localize and initialize a \texttt{Companion} from any
process running debugged code.
For that matter, a \texttt{PIDTable} is used to initialize and kill \texttt{Companion} processes
automatically.
This structure keeps a list of all debugged processes and their respective auxiliary processes,
alongside a count of function calls.
The debugged function calls notify their start to the \texttt{PIDTable}, getting a \texttt{Companion} 
process corresponding to its own PID.
If another debugged function call is made from the same process, the same previous \texttt{Companion}
will be assigned to it, pushing the previous process scope on a stack.
Upon the function call finish notification, the last process scope is popped, and \texttt{PIDTable} 
kills the \texttt{Companion} process if the scope stack is empty.

%% Controller
The \texttt{Controller} process provides an unified way to interface with the debugger,
providing access to information scattered across many \texttt{Companion} processes and the
\texttt{PIDTable}.
The API provided by this process is meant to enable the development of different interfaces
to the debugger, including remote debugging capabilities.

%% Protocol
The diagram on image \ref{protocol} illustrates the protocol used for communications 
between the various processes that constitute the debugger runtime. 

\begin{figure}[h!]
\centering
\includegraphics[width=0.95\textwidth]{protocol.pdf}
\caption{Overview of the debugger internal protocol.}
\label{protocol}
\end{figure}

\subsection{Implementation}
% Code injection 
The most sensible part of the implementation is the code injection mechanism, which should
handle some special cases of Elixir semantics.
The goal of the process is having the original code's AST being passed as parameter to
the \texttt{Runner.next} function, which crawls into branches, exchanges messages with
the \texttt{Companion} process, selectively recurs into branches and evaluates expressions.

%% Anonymous functions
One of the trickiest parts of code injection is the handling of anonymous functions, as those
may be called from non-debugged code when passed as parameter.
Those constructs are handled by inserting a nested \texttt{Runner.next} call to be evaluated
inside the current invocation.
This is the only point where the debugger makes an \texttt{:elixir.eval} call that
calls \texttt{:elixir.eval}, which may introduce debugger function calls on the client processes'
stacktrace.
The function \verb|(fn(x, y) -> 2 * x + 2 * y end)| results, after the debugger instrumentation,
in the following source code:
\begin{minted}[mathescape,
               fontsize=\footnotesize,
               linenos,
               numbersep=5pt,
               gobble=2,
               framesep=2mm]{elixir}
  fn(x, y) ->
    case(PIDTable.get()) do
      nil ->
        binding = Kernel.binding()
        scope = __ENV__ |> :elixir_scope.to_erl_env() |> Evaluator.update_binding(binding)
      companion ->
        state = Companion.get_state(companion)
        binding = Keyword.merge(state.binding(), Kernel.binding())
        scope = Evaluator.update_binding(state.scope(), binding)
    end
    module = __ENV__.module()
    scope = scope |> set_elem(20, __FILE__) |> set_elem(6, module) |> set_elem(14, module)
    comp = PIDTable.start(binding, scope)
    return = Runner.next(comp, {:+, 
                                [context: Elixir, import: Kernel], 
                                [{:*, 
                                  [context: Elixir, import: Kernel], 
                                  [2, {:x, [], Elixir}]}, 
                                 {:*, 
                                  [context: Elixir, import: Kernel], 
                                  [2, {:y, [], Elixir}]}]})
    PIDTable.finish()
    case(return) do
      {:ok, value} ->
        value
      {:exception, kind, reason, stacktrace} ->
        :erlang.raise(kind, reason, stacktrace)
    end
  end
\end{minted}
 
 
%% Emulated semantics
Getting control over some language constructs, specially those changing control-flow, requires
emulating their semantics using metaprogramming.
\texttt{receive}, \texttt{try}/\texttt{rescue} and \texttt{case} blocks are emulated through clauses 
that return their own body.
The names bound by the pattern matching of the clause are initialized as a second step, by re-evaluating
the matching clause after preparing the body on a similar way done with anonymous functions.
The binding and scope resulting of this final step is kept.

%% Escaped values 
Some basic Elixir datatypes such as function references can't be expressed through syntax, and their
inspection produces literals that can't be reintroduced in the AST and therefore need to be escaped.
Those values are escaped as special variables that are generated by the debugger, and their occurences on the
AST are replaced by variable accesses. 
This escaping process is be done in a order to allow debugged functions to interact with non-debugged
functions without producing escaped values.
The resulting variable names per datatype are depicted on Table \ref{escaped_data}.

\begin{table}
  \begin{tabular}{ l l l }
    \textbf{Datatype} & \textbf{Literal}            & \textbf{Variable name} \\
    \hline
    PIDs              & \verb|#PID<0.49.0>|         & \verb|__PID_0_49_0__|        \\
    References        & \verb|#Reference<0.0.0.76>| & \verb|__REF_0_0_0_76__|      \\
    Ports             & \verb|#Port<0.1287>|        & \verb|__PORT_0_1287__|       \\
    Closures          & \verb|-expr/5-fun-2-|       & \verb|__FUN_expr_5_fun_2__|  \\
  \end{tabular}
  \caption{Generated variable names for escaped data literals}
  \label{escaped_data}
\end{table}

%% OTP Principles
The \texttt{Companion}, \texttt{PIDTable} and \texttt{Controller} components of the debugger 
were implemented using OTP generic servers.
This module, available on Elixir as \texttt{GenServer.Behaviour}, implements client-server relation and
provides production-ready functionality for tracing and error reporting.
The use of generic servers also allowed those processes to fit on an OTP supervision tree, which
provides stability and error recovering capabilities to the debugger.

% CLI (IEx)
\subsection{Command Line Interface}
The debugger CLI is built on top of the \emph{Interactive Elixir} (\texttt{IEx}) shell, shipped with the
language runtime and compiler.
Aside from being a stable codebase, \texttt{IEx} also provides autocompletion, command-line history,
partial expression parsing, ANSI color support and a bunch of useful helpers.

%% Workflow
The CLI workflow starts with the \texttt{dbg/0} shell helper, which starts the debugger after prompting
the user for authorization.
On the root \texttt{dbg} shell, every issued command is evaluated on a new process, and differently
from the regular \texttt{IEx} shell, the returned value from the expression is the PID of the spawned
process.
By evaluating commands on separate processes it's possible to issue debug commands over function calls
issued by the user.
The shell itself is asynchronous, accepting more user input even if the last expression isn't evaluated yet.

%% Helpers
Most debugger functionalities are exposed through helper functions:
\begin{itemize}
  \item \texttt{db/0}: returns the list of current breakpoints.
  \item \texttt{db/1}: sets a list of breakpoints.
  \item \texttt{dc/2}: compiles a file at the given path for debugger debugging.
  \item \texttt{dl/1}: lists the current debugged processes.
  \item \texttt{ds/1}: starts a read-eval-print-loop (REPL) inside a given paused process.
  \item \texttt{dp/1}: pauses a process on the next expression to be evaluated.
  \item \texttt{du/1}: runs (unpauses) a process paused or at a breakpoint.
\end{itemize}

All helpers available on the regular \texttt{IEx} shell are available on the debug shell.
As some helpers (\texttt{ds/1}, \texttt{dr/1}, \texttt{dp/1}) expect PIDs as parameters, 
it's important to highlight the use of the \texttt{v/1} helper on the debugger to fetch 
identifiers for spawned processes using the shell line number.

The REPL started by the \texttt{ds/1} helper evaluates code inside the target process,
which is of great use on debugging distributed systems in Elixir.
This feature enables sending messages with the desired PID, generating references for 
a process, inspect values being manipulated by the code and even modify the process' names 
name binding.

% Debug sessions
\subsection{A Debug Session}
The following code implements the \texttt{pmap/3} function, which maps a function over a 
list in parallel, using the specified number of workers.
\begin{minted}[mathescape,
               fontsize=\footnotesize,
               linenos,
               numbersep=5pt,
               gobble=2,
               framesep=2mm]{elixir}
  defmodule Example do
    def pmap(list, workers, fun) when is_list(list) do
      size = Enum.count(list)
      chunk_size = round(size / workers)

      wlist = 0..(workers - 1)
      # generates jobs on the form { id, list }
      { jobs, [] } = Enum.reduce wlist, { [], list }, fn(w, { chunks, data }) ->
        { head, tail } = if w < workers - 1 do
          Enum.split(data, chunk_size)
        else
          { data, [] }
        end
        { [{ w, head } | chunks], tail }
      end

      # spawns workers
      joiner = self
      Enum.each jobs, fn(job) ->
        spawn fn ->
          { id, data } = job
          result = Enum.map(data, fn(x) -> fun.(x) end)
          joiner <- { id, result }
        end
      end

      # run joiner
      joiner(workers, [])
    end

    defp joiner(0, acc), do: List.flatten(acc)
    defp joiner(n, acc) do
      id = n - 1
      receive do
        { id, data } -> joiner(id, [data | acc])
      end
    end

    def random_array(count) do
      { a, b, c } = :erlang.now()
      :random.seed(a, b, c)
      Enum.map 0..(count - 1), fn(_) -> :random.uniform end
    end
  end
\end{minted}

This implementation has a bug, which we're going to find and solve using the debugger.
A transcript of the debugger session follows:

\begin{minted}[mathescape,
               fontsize=\footnotesize,
               numbersep=5pt,
               linenos,
               gobble=2,
               framesep=2mm]{iex}
    iex(1)> dbg
    Debug shell starting. Allow? [Yn] 
    {:done, #PID<0.27.0>}
    dbg(1)> dc "example.exs"
    [Example]
    dbg(2)> Example.pmap [1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000], 4, fn(x) -> 
    ...(2)>   Example.random\_array(x) 
    ...(2)> end
    <0.58.0>
    (3)<0.58.0> => [0.8026021975244177, 0.2903837494816779, 0.38109314359867774, 
    0.6830291865047566, 0.5039882822282824, 0.8214093453318134, 0.8623588179115824, 
    0.6168547766824757, 0.7625068107503787, 0.015184403984045414, 0.7247500438115153, 
    0.3445105101372117, 0.8378448530208509, 0.8724986720749133, 0.4967285442261491, 
    0.27212980714308466]
    dbg(3)> Example.pmap [1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000], 4, fn(x) -> 
    ...(3)>   Enum.count(Example.random\_array(x))
    ...(3)> end
    <0.69.0>
    (4)<0.69.0> => [1, 5, 10]
    dbg(4)> db "example.exs", 32
    [{"example.exs", 32}]
    dbg(5)> Example.pmap [1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000], 4, fn(x) -> 
    ...(5)>   Enum.count(Example.random\_array(x))
    ...(5)> end
    <0.81.0>
    hit breakpoint at #PID<0.81.0>: [{"example.exs", 32}]
    dbg(6)> dl
    #PID<0.88.0> "example.exs":41 running
    :random.uniform()
    #PID<0.90.0> "example.exs":22 running
    #PID<0.81.0> <- {0, [1, 5, 10]}
    #PID<0.72.0> "example.exs":41 running
    :random.uniform()
    #PID<0.86.0> "example.exs":41 running
    :random.uniform()
    #PID<0.74.0> "example.exs":41 running
    :random.uniform()
    #PID<0.81.0> "example.exs":32 paused
    n
    #PID<0.63.0> "example.exs":41 running
    :random.uniform()
    #PID<0.76.0> "example.exs":41 running
    :random.uniform()
    #PID<0.61.0> "example.exs":41 running
    :random.uniform()
    #PID<0.84.0> "example.exs":41 running
    :random.uniform()
    dbg(7)> du v 5
    :ok
    hit breakpoint at #PID<0.81.0>: [{"example.exs", 31}]
    dbg(8)> dl    
    #PID<0.72.0> "example.exs":41 running
    :random.uniform()
    #PID<0.86.0> "example.exs":41 running
    :random.uniform()
    #PID<0.74.0> "example.exs":41 running
    :random.uniform()
    #PID<0.81.0> "example.exs":32 paused
    4 - 1
    #PID<0.63.0> "example.exs":41 running
    :random.uniform()
    #PID<0.61.0> "example.exs":41 running
    :random.uniform()
    #PID<0.84.0> "example.exs":41 running
    :random.uniform()
    dbg(9)> du v 5
    :ok
    hit breakpoint at #PID<0.81.0>: [{"example.exs", 32}]
    dbg(10)> dl    
    #PID<0.72.0> "example.exs":41 running
    :random.uniform()
    #PID<0.86.0> "example.exs":41 running
    :random.uniform()
    #PID<0.74.0> "example.exs":41 running
    :random.uniform()
    #PID<0.81.0> "example.exs":32 paused
    id = 3
    #PID<0.63.0> "example.exs":41 running
    :random.uniform()
    #PID<0.61.0> "example.exs":41 running
    :random.uniform()
    #PID<0.84.0> "example.exs":41 running
    :random.uniform()
    dbg(11)> db []
    []
    dbg(12)> db "example.exs", 34                                                         
    [{"example.exs", 34}]
    dbg(13)> du v 5           
    :ok
    hit breakpoint at #PID<0.81.0>: [{"example.exs", 34}]
    dbg(14)> dl    
    #PID<0.72.0> "example.exs":41 running
    :random.uniform()
    #PID<0.86.0> "example.exs":41 running
    :random.uniform()
    #PID<0.74.0> "example.exs":41 running
    :random.uniform()
    #PID<0.81.0> "example.exs":34 paused
    id
    #PID<0.63.0> "example.exs":41 running
    :random.uniform()
    #PID<0.61.0> "example.exs":41 running
    :random.uniform()
    #PID<0.84.0> "example.exs":41 running
    :random.uniform()
    dbg(15)> ds v 5
    dbg:<0.81.0>(15)> id
    (15)<0.46.0> => 0
    dbg:<0.81.0>(16)> acc
    (16)<0.46.0> => []
    dbg(17)> dc "example.exs"
    example.exs:1: redefining module Example
    [Example]
    dbg(18)> Example.pmap [1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000], 4, fn(x) -> 
    ...(18)>   Enum.count(Example.random\_array(x))
    ...(18)> end
    <0.102.0>
    (18)<0.102.0> => [1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000]
\end{minted}

Comments about the session log by line number:
\begin{description}
  \item[1-4]\hfill \\
    Start the debugger shell and compile our example file.
  \item[6-8]\hfill \\
    Runs \texttt{pmap/3} with 4 workers.
  \item[10-14]\hfill \\
    The result is too short for the input list.
  \item[15-17]\hfill \\
    Add a \texttt{Enum.count/0} call to the previous \texttt{pmap} call.
  That function will return the length of the array, which should be the same list as the
  input for \texttt{pmap}.
  \item[19]\hfill \\
    The result is indeed much smaller. Our \texttt{pmap} implementation isn't
  returning data from all the workers.
  \item[20]\hfill \\
    To check the \texttt{id} value expected by the main process, a breakpoint is added 
  on the line 32 of \texttt{example.exs}.
  \item[22-26]\hfill \\
    The test function call is done again, and the process stops at the breakpoint.
  \item[27-47]\hfill \\
    The \texttt{dl} helper lists all debugged processes and their locations on the
  source code. The main process, with the PID \texttt{0.81.0}, is paused before evaluating the
  expression \texttt{n} on line 32. Many workers are still running, even those from the previous
  \texttt{pmap} invocations.
  \item[48-83]\hfill \\
    The process is paused by the breakpoint and unpaused by the \texttt{du} helper 
  until it reaches the desired expression, shown on line 77 of the transcript: \texttt{id = 3}.
  \item[83-87]\hfill \\
    The previous breakpoints are removed and a new one is set on line 34 of the 
  source file.
  \item[88-105]\hfill \\
    The process is ran again, and it stops on the next use of \texttt{id}, on line
  34 of \texttt{example.exs}. On that line, the value of that variable should be 3, as \texttt{joiner}
  will selectively receive processed data from the last to the first worker identifier.
  \item[106-110]\hfill \\
    By running a shell into the paused process, the values of \texttt{id} and \texttt{acc}
  are inspected. As \texttt{acc} is an empty list, \texttt{joiner} is well on its first \texttt{receive}.
  The wrong message is being received because the caret operator, that matches the value of variables on
  a pattern, is missing on the \texttt{id} use on line 34
  \item[114-118]\hfill \\
    After modifying line 34 of the source code to look like \verb_{ ^id, data } -> joiner(id, [data | acc])_,
  the file is recompiled and \texttt{pmap} is called again, this time yielding the correct result.
\end{description}

