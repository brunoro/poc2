\subsection{Elixir semantics}
% Semantics of Elixirs
%% SSA variables and pattern matching
Elixir is a strongly-typed message-oriented functional language with static single assignment variables.
Function definitions, \texttt{case} expressions and the \emph{match} operator(\texttt{=})
all boil down to pattern matching, and are the mechanisms that allow binding values to names in Elixir.
The use SSA variables means that Elixir has no mutable state, and \emph{reassigning} a variable $v$
in reality is splitting it into $v_0$ and $v_1$.

%% Exceptions
Error handling in Elixir involves the use of \texttt{try}/\texttt{rescue} blocks, which
are also based in pattern matching, and the \texttt{raise} primitive.
The \texttt{try} construct encloses a section of code where exceptions are captured, while the
\texttt{rescue} block specifies patterns of exceptions and how those should be handled.
\texttt{raise} is used to pass an exception up the call stack.
In order to interact with Erlang APIs, Elixir also provides the analogous \texttt{try}/\texttt{catch} blocks 
and the \texttt{throw} primitives, that work with Erlang flavored exceptions.

%% Functions
Functions in Elixir are defined by multiple clauses. When running code, the first matching 
clause of a function call will be used.
Function clauses can contain guards, which are expressions limited to comparison, boolean and arithmetic
operators, primitive type checking functions and a handful of other functions that handle PIDs
The identity of a functions defined inside modules is the (name, arity) tuple, so functions with 
the same name and different arities are treated as different functions.

Having separate namespaces for variables and functions, Elixir is a Lisp-2 language, so
special syntax is needed to call anonymous functions and pass named functions around.
The call operator(\texttt{fun.(arg1, arg2)}) is used to invoke anonymous function \texttt{fun}. 
The capture operator(\texttt{\&fun/2} or \texttt{\&fun(\&1, \&2)}) provides a way to pass named
functions as variables, either by stating its arity or by positioning the arguments.

%% Multiprocessing
The Erlang/Elixir agents concurrency model is based on five constructs: process identifiers(PIDs), references(Refs),
the \texttt{spawn} primitive, the send operator(\texttt{<-}) and the \texttt{receive} block.
PIDs and Refs are both unique and comparable identifiers for processes: PIDs are known by a process and its parent 
while Refs are created by a process using the \texttt{make\_ref/0} primitive.
New processes are created using the \texttt{spawn} primitive, which takes as argument a 0-arity function
that is invoked on a separated process.
The \texttt{<-} operator sends any data structure to a given process specified by a PID.
Messages received by a process are kept in a mailbox on the same order of receival. 
Those messages are consumed selectively using a \texttt{receive} block specifying the desired patterns.
If the process mailbox is empty, the \texttt{receive} block waits for a message to be received;
if no message pattern is matched, the receiving process hangs.
There are guarantees of message ordering only within one process, and 
\emph{delivery is guaranteed if nothing breaks}\cite{erlang-academic},
meaning that the according signals will be sent if any message isn't delivered.

%% Side-effects
A characteristic of the parallelism model adopted in Erlang/Elixir is \emph{parallel safety}\cite{erlang-rules}.
Side effects only happen on some controlled situations: sending and receiving messages and calling a built-in 
function that changes the running environiment.

%% Type system
As in Erlang, Elixir has optional type signatures used to catch type errors in compile time,
but the language itself is dynamic although strongly typed.
Type errors are thrown at runtime as exceptions, and no implicit type conversions are made.
Elixir type signatures are compatible with the Erlang tool \texttt{dialyser}, which can be used 
to detect a myriad of type errors through static analysis.

%% Protocols
A very high degree of extensibility can be achieved in Elixir with the use of polymorphism
by protocols.
Protocols define function signatures that can be satisfied by implementations for a given
type, which includes types defined by records.
Function calls are dispatched to the correct implementations at compile time, as those are
compiled to regular function clauses.
This mechanism allows implementing behaviour of functions for new data structures without 
interacting with the existing code.

%% Records
Elixir records are slightly different from Erlang ones.
They provide to the programmer facilities such as named fields, update methods and default values.
Records also can be used to specify protocols and type signatures.

%% Macros
Metaprogramming is a core feature of Elixir which allows users to extend the language through hygienic macros.
The macro system represents quoted expressions as a syntax tree of nested triples.
A great deal of flexibility is added to the language through metaprogramming, allowing, for instance, the creation
of custom function clause guards.
In fact, many of the standard library implementations are macros, including control-flow constructs such
as \texttt{if}/\texttt{else} and \texttt{cond}, which expand to \texttt{case} expressions at compile time.

% Core example
%% Macros, processes, messages
The following commented code example implements a function that computes a number 
of the Fibonacci sequence using memoization, illustrating Elixir's syntax and semantics:

\begin{minted}[mathescape,
               fontsize=\footnotesize,
               linenos,
               numbersep=5pt,
               gobble=2,
               framesep=2mm]{elixir}
# Defines a Memoize module.
  defmodule Memoize do
    # Allows Memoize.Table to be referred as Table.
    alias Memoize.Table
   
    # Defines a nested Table module.
    defmodule Table do
      # The `def` statement defines a function.
      # `when` defines guards to be matched on the function clause.
      def get_pid(name) when is_atom(name) do
        # The `case` block does pattern matching on a value.
        case Process.whereis(name) do
          nil -> 
            # The `spawn` primitive accepts a 0-arity anonymous function,
            # defined by the `fn` expression, and returns the PID of the new process.
            pid = spawn fn -> dict_loop(HashDict.new) end
            # `Process.register` allows atoms to be registered as process names.
            Process.register(pid, name) 
            pid
          pid -> 
            pid
        end
      end
   
      # `defp` defines a private function.
      defp dict_loop(dict) do
        # A `receive` block selectively receives messages by pattern-matching.
        receive do
          # `:get` is an atom.
          { :get, fun_call, caller } ->
            if Dict.has_key?(dict, fun_call) do
              # A tuple is being sent to `caller`. 
              # The `self` primitive gets a process' own PID.
              caller <- { self, { :hit, dict[fun_call] }}
            else
              caller <- { self, :miss }
            end
            dict_loop(dict)

          { :put, fun_call, result } ->
            dict_loop(Dict.put(dict, fun_call, result))
        end
      end

      def get(table, fun, args) do
        table <- { :get, { fun, args }, self }
        receive do
          # `^table` matches the contents of the `table` variable.
          { ^table, result } -> result
        end
      end
      def put(pid, fun, args, result) do
        pid <- { :put, { fun, args }, result }
      end
    end 

    def table_name(fun, args) do
      arity = Enum.count(args)
      # `#{}` performs string interpolation.
      # `|>` is the pipeline operator, which injects the results of the
      # previous function as the first argument of the next one.
      "#{fun}_#{inspect arity}" |> to_char_list |> list_to_atom
    end

    # `defmacro` defines a macro, which is a functions that transforms the AST. 
    defmacro defmem(header, do: body) do
      { fun, _meta, args } = header
      identifier = table_name(fun, args)

      # `quote` returns the quoted AST form of some code block.
      quote do
        # `unquote` inserts the value of a variable on a quoted block.
        def unquote(header) do
          table = Table.get_pid(unquote(identifier))
          case Table.get(table, unquote(fun), unquote(args)) do
            { :hit, result } ->
              result
            :miss ->
              result = unquote(body)
              Table.put(table, unquote(fun), unquote(args), result)
              result
          end
        end
      end
    end
  end
   
  defmodule Example do
    # `import` allows using public functions and macros 
    # from a module without a prefix.
    import Memoize
   
    defmem fibs(0), do: 0
    defmem fibs(1), do: 1
    defmem fibs(n), do: fibs(n - 1) + fibs(n - 2)
  end
\end{minted}

