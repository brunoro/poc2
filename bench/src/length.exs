import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule Length do
  defp len(l),          do: len(0, l)
  defp len(x, [_ | t]), do: len(x + 1, t)
  defp len(x, []),      do: x

  defp make_list(x),    do: make_list(x, [])
  defp make_list(0, l), do: l
  defp make_list(x, l), do: make_list(x - 1, [0 | l])

  defp loop(0, _, r), do: r
  defp loop(n, l, _), do: loop(n - 1, l, len(l))

  def start do
    l = make_list(2000)
    :erlang.statistics(:runtime)
    #r = loop(100000, l, 0)
    r = loop(1, l, 0)
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
    #IO.puts "runtime = #{inspect time} msecs"
    #IO.puts "result = #{inspect r}"
  end
end

Length.start
