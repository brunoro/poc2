import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule Fib do

  defp fib(0), do: 0
  defp fib(1), do: 1
  defp fib(x), do: fib(x - 1) + fib(x - 2)

  defp loop(0, r), do: r
  defp loop(n, _), do: loop(n - 1, fib(30))

  def start do
    :erlang.statistics(:runtime)
    #r = loop(50, 0)
    r = loop(1, 0)
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
    #IO.puts "runtime = #{inspect time} msecs"
    #IO.puts "result = #{inspect r}"
  end
end

Fib.start
