import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule Ring do
  defp process(main) do
    receive do
      dest -> process(main, dest)
    end
  end

  defp process(main, dest) do
    receive do
      :terminate -> 
        dest <- :terminate
      0 ->
        dest <- :terminate
        receive do
          :terminate ->
            main <- :done
        end
      x ->
        dest <- (x - 1)
        process(main, dest)
    end
  end

  defp create(_, 0),    do: []
  defp create(main, n), do: [spawn(fn -> process(main) end) | create(main, n - 1)]

  defp connect(ps),                 do: connect(hd(ps), ps)
  defp connect(first, [p]),         do: p <- first
  defp connect(first, [p | others]) do
    p <- hd(others)
    connect(first, others)
  end

  defp ring(nbprocs, hops) do
    ps = create(self, nbprocs)
    connect(ps)
    hd(ps) <- hops
    receive do
      :done -> :ok
    end
  end

  defp loop(0, r), do: r
  defp loop(n, _), do: loop(n - 1, ring(10, 100000))

  def start do
    :erlang.statistics(:runtime)
    #r = loop(100,0)
    r = loop(1,0)
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
    #IO.puts "runtime = #{inspect time} msecs"
    #IO.puts "result = #{inspect r}"
  end
end

Ring.start
