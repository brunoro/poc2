import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule QSort do
  defp qsort(l),          do: qsort(l, [])
  defp qsort([h | t], l), do: partition(h, t, [], [], l)
  defp qsort([], l),      do: l

  defp partition(pivot, [h | t], a, b, l) when h < pivot do
    partition(pivot, t, [h | a], b, l)
  end
  defp partition(pivot, [h | t], a, b, l) do
    partition(pivot, t, a, [h | b], l)
  end
  defp partition(pivot, [], a, b, l) do
    qsort(a, [pivot | qsort(b, l)])
  end

  defp loop(0, _, r), do: r
  defp loop(n, l, _), do: loop(n - 1, l, qsort(l))

  def start do
    l = [27,74,17,33,94,18,46,83,65, 2,
         32,53,28,85,99,47,28,82, 6,11,
         55,29,39,81,90,37,10, 0,66,51,
          7,21,85,27,31,63,75, 4,95,99,
         11,28,61,74,18,92,40,53,59, 8]
    :erlang.statistics(:runtime)
    #r = loop(50000, qsort(l), 0)
    r = loop(1, qsort(l), 0)
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
    #IO.puts "runtime = #{inspect time} msecs"
    #IO.puts "result = #{inspect r}"
  end
end

QSort.start
