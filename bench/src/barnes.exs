import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule Barnes do
  defp create_scenario(n, m) do
    do_create_scenario(0, 0, trunc(:math.sqrt(n)), m)
  end

  defp do_create_scenario(_, sn, sn, _), do: []
  defp do_create_scenario(sn, y, sn, m),  do: do_create_scenario(0, y + 1, sn, m)
  defp do_create_scenario(x, y, sn, m) do
    xpos0 = (((20000.0 * 2) / sn) * x) - 20000.0
    ypos0 = (((20000.0 * 2) / sn) * y) - 20000.0
    calibrate = ((20000.0 * 2) / sn) / 2
    xpos = xpos0 + calibrate
    ypos = ypos0 + calibrate
    [{m, xpos, ypos, 0.0, 0.0} | do_create_scenario(x + 1, y, sn, m)]
  end

  defp relpos_to_quadrant(dx, dy) when dx >= 0 do
    if dy >= 0 do 0 else 3 end
  end
  defp relpos_to_quadrant(_ , dy) do
    if dy >= 0 do 1 else 2 end
  end

  defp quadrant_to_dx(0, d), do: d
  defp quadrant_to_dx(1, d), do: -d
  defp quadrant_to_dx(2, d), do: -d
  defp quadrant_to_dx(3, d), do: d
 
  defp quadrant_to_dy(q, d) do 
    if q < 2 do d else -d end
  end

  defp create_tree(stars), do: do_create_tree(stars, :empty)

  defp do_create_tree([], tree), do: tree
  defp do_create_tree([{ m, x, y, _, _ } | stars], tree) do
    do_create_tree(stars, insert_tree_element(tree, m, x, y, 0.0, 0.0, 20000.0))
  end

  defp insert_tree_element(:empty, m, x, y, _, _, _), do: { :body, m, x, y }
  defp insert_tree_element({ :branch, m0, sub_tree }, m, x, y, ox, oy, d) do
    q = relpos_to_quadrant(x - ox, y - oy)
    d2 = d / 2
    dx = quadrant_to_dx(q, d2)
    dy = quadrant_to_dy(q, d2)
    nt = insert_tree_element(elem(sub_tree, q), m, x, y, ox + dx, oy + dy, d2)
    { :branch, m0 + m, set_elem(sub_tree, q, nt) }
  end
  defp insert_tree_element({ :body, m0, x0, y0 }, m, x, y, ox, oy, d) do
    resolve_body_conflict(m, x, y, m0, x0, y0, ox, oy, d)
  end

  defp resolve_body_conflict(m0, x0, y0, m1, x1, y1, ox, oy, d) do
    t = { :empty, :empty, :empty, :empty }
    q0 = relpos_to_quadrant(x0 - ox, y0 - oy)
    q1 = relpos_to_quadrant(x1 - ox, y1 - oy)
    d2 = d / 2
    if q0 == q1 do
      dx = quadrant_to_dx(q0, d2)
      dy = quadrant_to_dy(q1, d2)
      nt = resolve_body_conflict(m0, x0, y0, m1, x1, y1, ox + dx, oy + dy, d2)
      { :branch, m0 + m1, set_elem(t, q0, nt) }
    else 
      nt = set_elem(t, q0, { :body, m0, x0, y0 })
      { :branch, m0 + m1, set_elem(nt, q1, { :body, m1, x1, y1 }) }
    end
  end

  defp compute_acceleration(:empty, _, _, _, _, _), do: { 0.0, 0.0 }
  defp compute_acceleration({ :body, bm, bx, by }, _, _, _, x, y) do
    dx = bx - x
    dy = by - y
    r2 = (dx * dx) + (dy * dy)
    divisor = r2 * :math.sqrt(r2)
    # was: divisor < @epsilon 
    if divisor < 0.000001 do 
        { 0.0, 0.0 }
      else
        expr = bm / divisor
        { dx * expr, dy * expr }
    end
  end
  defp compute_acceleration({:branch, m, sub_tree}, d, ox, oy, x, y) do
    dx = ox - x
    dy = oy - y
    r2 = (dx * dx) + (dy * dy)
    dd = d * d
    # was: r2_theta2 = @theta2 * r2
    r2_theta2 = 0.09 * r2 
    # ok to approximate?
    if dd < r2_theta2 do
      divisor = r2 * :math.sqrt(r2)
      if divisor < 0.000001 do
        { 0.0, 0.0 }
      else
        expr = m / divisor
        { dx * expr, dy * expr }
      end
    # not ok to approximate...
    else
      d2 = d / 2
      { ax0, ay0 } = compute_acceleration(elem(sub_tree, 0),
                                        d2, ox + quadrant_to_dx(0, d2),
                                        oy + quadrant_to_dy(0 ,d2), x, y)
      { ax1, ay1 } = compute_acceleration(elem(sub_tree, 1),
                                        d2, ox + quadrant_to_dx(1, d2),
                                        oy + quadrant_to_dy(1, d2), x, y)
      { ax2, ay2 } = compute_acceleration(elem(sub_tree, 2),
                                        d2, ox + quadrant_to_dx(2, d2),
                                        oy + quadrant_to_dy(2, d2), x, y)
      { ax3, ay3 } = compute_acceleration(elem(sub_tree, 3),
                                        d2, ox + quadrant_to_dx(3, d2),
                                        oy + quadrant_to_dy(3, d2), x, y)
      { ax0 + ax1 + ax2 + ax3, ay0 + ay1 + ay2 + ay3 }
    end
  end

  defp compute_star_accelerations(_, []), do: []
  defp compute_star_accelerations(tree, [{ _, x, y, _, _} | stars]) do
    a = compute_acceleration(tree, 20000.0, 0.0, 0.0, x, y)
    b = compute_star_accelerations(tree, stars)
    [a | b]
  end

  defp compute_next_state( [], _, _), do: []
  defp compute_next_state([{ m, x, y, vx, vy } | stars], [{ ax, ay } | accs], time) do
    vx0 = vx + (ax * time)
    vy0 = vy + (ay * time)
    [{ m, x + (vx * time), y + (vy * time), vx0, vy0 } | compute_next_state(stars, accs, time)]
  end

  defp advance_time(time,stars) do
    tree = create_tree(stars)
    acc = compute_star_accelerations(tree, stars)
    compute_next_state(stars, acc, time)
  end

  defp loop(0, _, stars),    do: stars
  defp loop(n, time, stars), do: loop(n - 1, time, advance_time(time, stars))

  def start do
    stars = create_scenario(1000, 1.0)
    :erlang.statistics(:runtime)
    #r = hd(loop(10,1000.0,stars))
    r = hd(loop(1,1000.0,stars))
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
  end
end

Barnes.start
