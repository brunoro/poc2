import IEx.Debugger
IEx.Debugger.start([])

defdebugmodule NRev do
  defp nrev([h | t]), do: app(nrev(t), [h])
  defp nrev([]),      do: []

  defp app([h | t], l), do: [h | app(t, l)]
  defp app([], l),      do: l

  defp iota(n),    do: iota(n, [])
  defp iota(0, l), do: l
  defp iota(n, l), do: iota(n - 1, [n | l])

  defp loop(0, _, r), do: r
  defp loop(n, l, _), do: loop(n - 1, l, nrev(l))

  def start do
    l = iota(100)
    :erlang.statistics(:runtime)
    #r = loop(20000, l, 0)
    r = loop(1, l, 0)
    { _, time } = :erlang.statistics(:runtime)
    IO.inspect time
    #IO.puts "runtime = #{inspect time} msecs"
    #IO.puts "result = #{inspect r}"
  end
end

NRev.start
