for EXS in $(ls src/*.exs)
do
  echo $EXS
  ./run.sh $EXS | awk '{delta = $1 - avg; avg += delta / NR; mean2 += delta * ($1 - avg); } END { print avg; print sqrt(mean2 / NR); }'
done
