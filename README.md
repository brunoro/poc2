# POC2 DCC/UFMG - A Debugger Based on Metaprogramming for Elixir

## Projeto orientado em computação 2 (Bachelor thesis)

Project developed initially under a Google Summer of Code scholarship.
Working source code for the Elixir debugger is available at my Elixir fork at github: https://github.com/brunoro/elixir