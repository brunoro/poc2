# Defines a Memoize module.
defmodule Memoize do
  # Allows Memoize.Table to be referred as Table.
  alias Memoize.Table
 
  # Defines a nested Table module.
  defmodule Table do
    # The `def` statement defines a function.
    # `when` defines guards to be matched on the function clause.
    def get_pid(name) when is_atom(name) do
      # The `case` block does pattern matching on a value.
      case Process.whereis(name) do
        nil -> 
          # The `spawn` primitive accepts a 0-arity anonymous function,
          # defined by the `fn` expression, and returns the PID of the new process.
          pid = spawn fn -> dict_loop(HashDict.new) end
          # `Process.register` allows atoms to be registered as process names.
          Process.register(pid, name) 
          pid
        pid -> 
          pid
      end
    end
 
    # `defp` defines a private function.
    defp dict_loop(dict) do
      # A `receive` block selectively receives messages by pattern-matching.
      receive do
        # `:get` is an atom.
        { :get, fun_call, caller } ->
          if Dict.has_key?(dict, fun_call) do
            # A tuple is being sent to `caller`. 
            # The `self` primitive gets a process' own PID.
            caller <- { self, { :hit, dict[fun_call] }}
          else
            caller <- { self, :miss }
          end
          dict_loop(dict)

        { :put, fun_call, result } ->
          dict_loop(Dict.put(dict, fun_call, result))
      end
    end

    def get(table, fun, args) do
      table <- { :get, { fun, args }, self }
      receive do
        # `^table` matches the contents of the `table` variable.
        { ^table, result } -> result
      end
    end
    def put(pid, fun, args, result) do
      pid <- { :put, { fun, args }, result }
    end
  end 

  def table_name(fun, args) do
    arity = Enum.count(args)
    # `#{}` performs string interpolation.
    # `|>` is the pipeline operator, which injects the results of the
    # previous function as the first argument of the next one.
    "#{fun}_#{inspect arity}" |> to_char_list |> list_to_atom
  end

  # `defmacro` defines a macro, which is a functions that transforms the AST. 
  defmacro defmem(header, do: body) do
    { fun, _meta, args } = header
    identifier = table_name(fun, args)

    # `quote` returns the quoted AST form of some code block.
    quote do
      # `unquote` inserts the value of a variable on a quoted block.
      def unquote(header) do
        table = Table.get_pid(unquote(identifier))
        case Table.get(table, unquote(fun), unquote(args)) do
          { :hit, result } ->
            result
          :miss ->
            result = unquote(body)
            Table.put(table, unquote(fun), unquote(args), result)
            result
        end
      end
    end
  end
end
 
defmodule Example do
  # `import` allows using public functions and macros 
  # from a module without a prefix.
  import Memoize
 
  defmem fibs(0), do: 0
  defmem fibs(1), do: 1
  defmem fibs(n), do: fibs(n - 1) + fibs(n - 2)
end

IO.inspect Example.fibs(10)
