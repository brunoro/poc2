defmodule Example do
  def pmap(coll, workers, fun) do
    size = Enum.count(coll)
    chunk_size = round(size / workers)

    wlist = 0..(workers - 1)
    # generates jobs on the form { id, coll }
    { jobs, [] } = Enum.reduce wlist, { [], coll }, fn(w, { chunks, data }) ->
      { head, tail } = if w < workers - 1 do
        Enum.split(data, chunk_size)
      else
        { data, [] }
      end
      { [{ w, head } | chunks], tail }
    end

    # spawns workers
    joiner = self
    Enum.each jobs, fn(job) ->
      spawn fn ->
        { id, data } = job
        result = Enum.map(data, fn(x) -> fun.(x) end)
        joiner <- { id, result }
      end
    end

    # run joiner
    joiner(workers, [])
  end

  defp joiner(0, acc), do: List.flatten(acc)
  defp joiner(n, acc) do
    id = n - 1
    receive do
      { id, data } -> joiner(id, [data | acc])
    end
  end
end

#IO.inspect Example.pmap 0..100, 4, fn(x) -> 
#  :math.pow(x, 2) 
#end
